<?php

namespace App\Actions;

use Carbon\Carbon;
use App\Models\Task;
use App\Http\Traits\getUserInfo;


class FilterTaskAction
{
    use getUserInfo;

    public function handle($validData, $request)
    {
        $tasks = Task::whereUserId($this->getUserIdByToken($request));

        if (!empty($validData)) {
            foreach ($validData as $param => $data) {
                if ($param == 'finish_date') {
                    $data = Carbon::parse($data)->format("Y-m-d");
                }

                $tasks->where($param, '=', $data);
            }
        }

        return $tasks->get();
    }
}
