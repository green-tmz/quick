<?php

namespace App\Http\Requests;

use App\Http\Traits\getUserInfo;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Traits\RequestFailedValidationTrait;

class TaskRequest extends FormRequest
{
    use RequestFailedValidationTrait, getUserInfo;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|min:2',
            'status_id' => 'required|integer|exists:App\Models\Status,id',
            'finish_date' => 'required|date_format:d.m.Y',
            'desc' => 'sometimes|string|min:2|max:255',
            'user_id' => 'integer'
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'user_id' => $this->getUserIdByToken($this)
        ]);
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'name.required' => 'Поле обязательно для заполнения',
            'name.min' => 'Поле должно содержать не менее 2х символов',
            'status_id.required' => 'Поле обязательно для заполнения',
            'status_id.integer' => 'Поле должно быть целым числом',
            'status_id.exists' => 'Id статуса не найдено',
            'finish_date.required' => 'Поле обязательно для заполнения',
            'finish_date.date_format' => 'Поле должно быть в формате дд.мм.гггг',
            'desc.string' => 'Описание должно быть текстом',
            'desc.min' => 'Описание должно содержать не менее 2х символов',
            'desc.max' => 'Описание должно содержать не более 255х символов',
        ];
    }
}
