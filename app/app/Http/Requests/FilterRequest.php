<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Traits\RequestFailedValidationTrait;

class FilterRequest extends FormRequest
{
    use RequestFailedValidationTrait;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'status_id' => 'sometimes|integer|exists:App\Models\Status,id',
            'finish_date' => 'sometimes|date_format:d.m.Y',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'status_id.integer' => 'Поле должно быть целым числом',
            'status_id.exists' => 'Id статуса не найдено',
            'finish_date.required' => 'Поле обязательно для заполнения',
            'finish_date.date_format' => 'Поле должно быть в формате дд.мм.гггг',
        ];
    }
}
