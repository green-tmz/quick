<?php

namespace App\Http\Traits;

use Illuminate\Http\Request;
use Laravel\Sanctum\PersonalAccessToken;

trait getUserInfo
{
    public function getUserIdByToken(Request $request)
    {
        $authData = explode(" ", $request->header('authorization'));

        $token = PersonalAccessToken::findToken($authData[1]);
        $user = $token->tokenable;

        return $user->id;
    }
}
