<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\RegisterRequest;

class AuthController extends Controller
{
    const EXIT_TIME = 30;

    /**
     * Авторизация пользователя в системе.
     *
     * @OA\Post(
     *      path="/api/login",
     *      summary="Авторизация пользователя",
     *      description="Для авторизации нужен email и пароль пользователя",
     *      operationId="authLogin",
     *      tags={"Авторизация"},
     *      @OA\RequestBody(
     *          required=true,
     *          description="Значения общих настроек",
     *          @OA\JsonContent(
     *              @OA\Property(property="email", description="Email", type="string", example="user@user.ru"),
     *              @OA\Property(property="password", description="Пароль", type="string", example="password123")
     *          ),
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Неверные авторизационные данные",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="error",
     *                  type="object",
     *                  @OA\Property(property="error_code", type="integer", example="2"),
     *                  @OA\Property(property="error_msg", type="string", example="Неверные авторизационные данные.")
     *              ),
     *          ),
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Ошибка валидации",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="error",
     *                  type="object",
     *                  @OA\Property(property="error_code", type="integer", example="1"),
     *                  @OA\Property(property="error_msg", type="string", example="Ошибка валидации."),
     *                  @OA\Property(
     *                      property="fields",
     *                      type="object",
     *                      @OA\Property(property="email", type="array", @OA\Items(example="Поле email должно содержать корректный email.")),
     *                  )
     *              )
     *          )
     *      )
     * )
     *
     * @param  LoginRequest  $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        if (!Auth::attempt($request->validated())) {
            return response()->json([
                'error' => [
                    'error_code' => 2,
                    'error_msg' => 'Неверные авторизационные данные.',
                ],
            ], 401);
        }

        return $this->createToken();
    }

    /**
     * Регистрация пользователя в системе.
     *
     * @OA\Post(
     *      path="/api/register",
     *      summary="Регистрация пользователя",
     *      description="Для регистрации нужен email и пароль пользователя",
     *      operationId="authRegister",
     *      tags={"Авторизация"},
     *      @OA\RequestBody(
     *          required=true,
     *          description="Значения общих настроек",
     *          @OA\JsonContent(
     *              @OA\Property(property="name", description="Name", type="string", example="User"),
     *              @OA\Property(property="email", description="Email", type="string", example="user@user.ru"),
     *              @OA\Property(property="password", description="Пароль", type="string", example="password123"),
     *              @OA\Property(property="password_confirmation", description="Подтверждение пароля", type="string", example="password123")
     *          ),
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Неверные авторизационные данные",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="error",
     *                  type="object",
     *                  @OA\Property(property="error_code", type="integer", example="2"),
     *                  @OA\Property(property="error_msg", type="string", example="Неверные авторизационные данные.")
     *              ),
     *          ),
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Ошибка валидации",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="error",
     *                  type="object",
     *                  @OA\Property(property="error_code", type="integer", example="1"),
     *                  @OA\Property(property="error_msg", type="string", example="Ошибка валидации."),
     *                  @OA\Property(
     *                      property="fields",
     *                      type="object",
     *                      @OA\Property(property="email", type="array", @OA\Items(example="Поле email должно содержать корректный email.")),
     *                  )
     *              )
     *          )
     *      )
     * )
     *
     * @return JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        $user = User::create($request->validated());

        return response()->json([
            'user' => [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
            ],
        ]);
    }

    /**
     * Создание токена для авторизованного пользователя.
     *
     * @param  bool  $isMobile
     * @return JsonResponse
     */
    public function createToken(): JsonResponse
    {
        /** @var User $user */
        $user = auth()->user();

        $user->tokens()
            ->where('name', 'auth_token')
            ->where('last_used_at', '<', Carbon::now()->modify("-".self::EXIT_TIME." minutes"))
            ->delete();

        $newToken = $user->createToken('auth_token')->plainTextToken;

        /** @var PersonalAccessToken $accessToken */
        $accessToken = $user->tokens()->where('name', 'accessToken')->first();
        if (empty($accessToken)) {
            $user->createToken('accessToken');
        }

        return response()->json([
            'data' => [
                'token' => $newToken,
                'lifetime' => self::EXIT_TIME,
                'user' => [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                ]
            ],
        ])->withHeaders([
            'Access-Control-Allow-Origin' => '*'
        ]);
    }
}
