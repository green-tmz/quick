<?php

namespace App\Http\Controllers;

use App\Models\Status;
use App\Http\Resources\StatusCollection;

class StatusController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/statuses",
     *      summary="Список статусов задач",
     *      description="Возвращает список статусов задач",
     *      operationId="statusesList",
     *      tags={"Статусы задач"},
     *      security={ {"sanctum": {} }},
     *      @OA\Response(
     *          response=200,
     *          description="Запрос выполнен успешно",
     *      )
     * )
     *
     * @return mixed
     */
    public function index()
    {
        return StatusCollection::collection(Status::all());
    }
}
