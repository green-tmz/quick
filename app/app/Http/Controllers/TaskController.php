<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use App\Actions\FilterTaskAction;
use App\Http\Requests\TaskRequest;
use App\Http\Requests\FilterRequest;
use App\Http\Resources\TaskResource;
use App\Http\Resources\TaskCollection;

class TaskController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/tasks",
     *      summary="Список своих задач",
     *      summary="Возвращает список своих задач",
     *      description="Возвращает список своих задач",
     *      operationId="tasksList",
     *      security={ {"sanctum": {} }},
     *      tags={"Задачи"},
     *      @OA\Parameter(
     *          description="id статуса",
     *          in="query",
     *          name="status_id",
     *          example="2"
     *      ),
     *      @OA\Parameter(
     *          description="Крайний срок завершения",
     *          in="query",
     *          name="finish_date",
     *          example="03.07.2024"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Запрос выполнен успешно",
     *      )
     * )
     *
     * @return mixed
     */
    public function index(FilterRequest $request, FilterTaskAction $action)
    {
        $validData = $request->validated();
        return TaskCollection::collection($action->handle($validData, $request));
    }

    /**
     * Создание задачи.
     *
     * @OA\Post(
     *     path="/api/tasks",
     *     operationId="tasksStore",
     *     tags={"Задачи"},
     *     summary="Создание задачи",
     *     description="Создание задачи",
     *     security={ {"sanctum": {} }},
     *     @OA\Parameter(
     *         description="Название",
     *         in="query",
     *         name="name",
     *         required=true,
     *         example="Задача 1"
     *     ),
     *     @OA\Parameter(
     *         description="id статуса",
     *         in="query",
     *         name="status_id",
     *         required=true,
     *         example="2"
     *     ),
     *     @OA\Parameter(
     *         description="Описание",
     *         in="query",
     *         name="desc",
     *         example="Описание задачи 1"
     *     ),
     *     @OA\Parameter(
     *         description="Крайний срок завершения",
     *         in="query",
     *         name="finish_date",
     *         required=true,
     *         example="03.07.2024"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Запрос выполнен успешно",
     *     )
     * )
     *
     */
    public function store(TaskRequest $request)
    {
        $task = Task::create($request->validated());
        return new TaskResource($task);
    }

    /**
     * Информация о задаче.
     *
     * @OA\Get(
     *     path="/api/tasks/{task}",
     *     operationId="tasksEdit",
     *     tags={"Задачи"},
     *     summary="Информация о задаче",
     *     description="Информация о задаче",
     *     security={ {"sanctum": {} }},
     *     @OA\Parameter(
     *         description="id задачи",
     *         in="path",
     *         name="task",
     *         required=true,
     *         example="1"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Запрос выполнен успешно",
     *     )
     * )
     *
     */
    public function show(Task $task)
    {
        return new TaskResource($task);
    }

    /**
     * Обновление задачи.
     *
     * @OA\Put(
     *     path="/api/tasks/{task}",
     *     operationId="tasksUpdate",
     *     tags={"Задачи"},
     *     summary="Обновление задачи",
     *     description="Обновление задачи",
     *     security={ {"sanctum": {} }},
     *     @OA\Parameter(
     *         description="id задачи",
     *         in="path",
     *         name="task",
     *         required=true,
     *         example="1"
     *     ),
     *     @OA\Parameter(
     *         description="Название",
     *         in="query",
     *         name="name",
     *         required=true,
     *         example="Задача 1"
     *     ),
     *     @OA\Parameter(
     *         description="id статуса",
     *         in="query",
     *         name="status_id",
     *         required=true,
     *         example="2"
     *     ),
     *     @OA\Parameter(
     *         description="Описание",
     *         in="query",
     *         name="desc",
     *         example="Описание задачи 1"
     *     ),
     *     @OA\Parameter(
     *         description="Крайний срок завершения",
     *         in="query",
     *         name="finish_date",
     *         required=true,
     *         example="03.07.2024"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Запрос выполнен успешно",
     *     )
     * )
     *
     */
    public function update(TaskRequest $request, Task $task)
    {
        $task->update($request->validated());
        return new TaskResource($task);
    }

    /**
     * Удаление задачи.
     *
     * @OA\Delete(
     *     path="/api/tasks/{task}",
     *     operationId="tasksDestroy",
     *     tags={"Задачи"},
     *     summary="Удаление задачи",
     *     description="Удаление задачи",
     *     security={ {"sanctum": {} }},
     *     @OA\Parameter(
     *         description="id задачи",
     *         in="path",
     *         name="task",
     *         required=true,
     *         example="1"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Запрос выполнен успешно",
     *     )
     * )
     *
     */
    public function destroy(Task $task, Request $request, FilterTaskAction $action)
    {
        $task->delete();
        return $this->index($request, $action);
    }
}
