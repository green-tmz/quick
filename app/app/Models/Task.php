<?php

namespace App\Models;

use App\Http\Traits\getUserInfo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Task extends Model
{
    use HasFactory, getUserInfo;

    protected $fillable = [
        'name', 'status_id', 'user_id', 'desc', 'finish_date'
    ];

    protected $casts = [
        'finish_date' => 'date',
    ];

    public function status(): HasOne
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    public function authorData(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
